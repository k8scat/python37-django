FROM python:3.7-alpine
LABEL maintainer="hsowan <hsowan.me@gmail.com>"
ENV TZ=Asia/Shanghai
WORKDIR /data
COPY requirements.txt .
RUN apk add --no-cache mariadb-dev gcc && \
    pip install -r requirements.txt
